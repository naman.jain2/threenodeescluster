#Genearating ec2 keypair
resource "tls_private_key" "elk" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "aws_key_pair" "elk" {
  key_name   = "elk"
  public_key = tls_private_key.elk.public_key_openssh
}

resource "aws_s3_bucket_object" "ssh_key" {
  bucket  = "namanjain-terraform-tfstate6"
  key     = "ssh/elk.pem"
  content = tls_private_key.elk.private_key_pem
}
resource "local_file" "local_ssh_private_key" {
  content         = tls_private_key.elk.private_key_pem
  filename        = "elk"
  file_permission = "644"
}

resource "local_file" "local_ssh_public_key" {
  content         = tls_private_key.elk.public_key_openssh
  filename        = "elk.pub"
  file_permission = "600"
} 