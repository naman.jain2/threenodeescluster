sudo apt-get update
sudo apt-get install default-jre -y
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
sudo apt-get update
sudo apt-get install kibana
sudo cat <<EOF > /etc/kibana/kibana.yml
server.port: 5601
server.host: "KIBANA_PRIVATE_IP"
elasticsearch.hosts: ["http://MASTER_PRIVATE_IP:9200"]
EOF
sudo systemctl start kibana
sudo systemctl enable kibana
