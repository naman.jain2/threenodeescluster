#!/bin/bash
sudo apt update
sudo apt-get install default-jre -y
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
sudo apt-get update
sudo apt-get install elasticsearch
sudo cat <<EOF > /etc/elasticsearch/elasticsearch.yml
cluster.name: my-cluster
node.name: "master-node"
node.master: true
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
network.host: MASTER_PRIVATE_IP
http.port: 9200
discovery.zen.ping.unicast.hosts: ["MASTER_PRIVATE_IP","DATA-NODE1_PRIVATE_IP","DATA-NODE2_PRIVATE_IP"]

EOF

sudo cat <<EOF >> /etc/elasticsearch/jvm.options
-Xms500m
-Xmx500m
EOF


sudo cat <<EOF >> /etc/default/elasticsearch
MAX_LOCKED_MEMORY=unlimited
EOF


sudo cat <<EOF >> /etc/sysctl.conf
vm.max_map_count=262144
EOF

sudo cat <<EOF >> /etc/security/limits.conf
- nofile 65536
EOF

sudo systemctl start elasticsearch
sudo systemctl enable elasticsearch

