output "kibana_ip" {
  value = "${aws_instance.kibana.public_ip}:5601"
}
output "master-node_ip" {
  value = "${aws_instance.master-node.public_ip}:9200"
}
output "datanode1_ip" {
  value = "${aws_instance.data-node1.public_ip}:9200"
}
output "datanode2_ip" {
  value = "${aws_instance.data-node2.public_ip}:9200"
}

