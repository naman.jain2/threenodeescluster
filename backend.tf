terraform {
  backend "s3" {
    bucket = "namanjain-terraform-tfstate6"
    key    = "terraform/state/elk"
    region = "us-east-1"
  }
}
