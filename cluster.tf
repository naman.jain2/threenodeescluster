
resource "aws_instance" "master-node" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  key_name      = aws_key_pair.elk.key_name
  vpc_security_group_ids = [aws_security_group.ES-SG.id]
  provisioner "file" {
    source      = "master.sh"
    destination = "/tmp/script.sh"
  }
  connection {
    host        = aws_instance.master-node.public_ip
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }
  tags = {
    Name = "master-node"
  }
}
resource "aws_instance" "data-node1" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  key_name      = aws_key_pair.elk.key_name
vpc_security_group_ids = [aws_security_group.ES-SG.id]
  provisioner "file" {
    source      = "dataNode1.sh"
    destination = "/tmp/script.sh"
  }
  connection {
    host        = aws_instance.data-node1.public_ip
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }
  tags = {
    Name = "data-node-1"
  }
}
resource "aws_instance" "kibana" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  key_name      = aws_key_pair.elk.key_name
vpc_security_group_ids = [aws_security_group.Kibana-SG.id]
  provisioner "file" {
    source      = "kibana-userdata.sh"
    destination = "/tmp/script.sh"
  }
  connection {
    host        = aws_instance.kibana.public_ip
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }
  tags = {
    Name = "Kibana"
  }
}
resource "aws_instance" "data-node2" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  key_name      = aws_key_pair.elk.key_name
vpc_security_group_ids = [aws_security_group.ES-SG.id]
  provisioner "file" {
    source      = "dataNode2.sh"
    destination = "/tmp/script.sh"
  }
  connection {
    host        = aws_instance.data-node2.public_ip
    type        = "ssh"
    user        = var.INSTANCE_USERNAME
    private_key = file(var.PATH_TO_PRIVATE_KEY)
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sed -i 's/MASTER_PRIVATE_IP/${aws_instance.master-node.private_ip}/g; s/DATA-NODE1_PRIVATE_IP/${aws_instance.data-node1.private_ip}/g; s/DATA-NODE2_PRIVATE_IP/${aws_instance.data-node2.private_ip}/g;' /tmp/script.sh",
      "sudo sed -i -e 's/\r$//' /tmp/script.sh", # Remove the spurious CR characters.
      "sudo /tmp/script.sh",
    ]

    connection {
      host        = aws_instance.master-node.public_ip
      type        = "ssh"
      user        = var.INSTANCE_USERNAME
      private_key = file(var.PATH_TO_PRIVATE_KEY)
    }
  }

  # Provisioner file for Second instance  


  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sed -i 's/MASTER_PRIVATE_IP/${aws_instance.master-node.private_ip}/g; s/DATA-NODE1_PRIVATE_IP/${aws_instance.data-node1.private_ip}/g; s/DATA-NODE2_PRIVATE_IP/${aws_instance.data-node2.private_ip}/g;' /tmp/script.sh",
      "sudo sed -i -e 's/\r$//' /tmp/script.sh", # Remove the spurious CR characters.
      "sudo /tmp/script.sh",
    ]

    connection {
      host        = aws_instance.data-node1.public_ip
      type        = "ssh"
      user        = var.INSTANCE_USERNAME
      private_key = file(var.PATH_TO_PRIVATE_KEY)
    }
  }

  # Provisioner file for Third instance  

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sed -i 's/MASTER_PRIVATE_IP/${aws_instance.master-node.private_ip}/g; s/DATA-NODE1_PRIVATE_IP/${aws_instance.data-node1.private_ip}/g; s/DATA-NODE2_PRIVATE_IP/${aws_instance.data-node2.private_ip}/g;' /tmp/script.sh",
      "sudo sed -i -e 's/\r$//' /tmp/script.sh", # Remove the spurious CR characters.
      "sudo /tmp/script.sh",
    ]

    connection {
      host        = aws_instance.data-node2.public_ip
      type        = "ssh"
      user        = var.INSTANCE_USERNAME
      private_key = file(var.PATH_TO_PRIVATE_KEY)
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sed -i 's/MASTER_PRIVATE_IP/${aws_instance.master-node.private_ip}/g; s/KIBANA_PRIVATE_IP/${aws_instance.kibana.private_ip}/g; s/DATA-NODE2_PRIVATE_IP/${aws_instance.data-node2.private_ip}/g;' /tmp/script.sh",
      "sudo sed -i -e 's/\r$//' /tmp/script.sh", # Remove the spurious CR characters.
      "sudo /tmp/script.sh",
    ]

    connection {
      host        = aws_instance.kibana.public_ip
      type        = "ssh"
      user        = var.INSTANCE_USERNAME
      private_key = file(var.PATH_TO_PRIVATE_KEY)
    }
  }
  tags = {
    Name = "data-node-2"
  }
}

